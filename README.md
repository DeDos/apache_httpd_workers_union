![Worker Union MPM](worker_union_mpm.png)
**Apache Worker Union MPM**

This repository contains patches that provide more control over how [Apache](https://en.wikipedia.org/wiki/Apache_HTTP_Server)
computes responses to requests.

The patches work by changing the thread-pool structure for Apache's [Worker MPM](https://httpd.apache.org/docs/2.4/mod/worker.html) as
described in our [FEAST'18 paper](https://www.seas.upenn.edu/~nsultana/files/feast.pdf).
The different patches provide additional features or flexibility.

# Building and configuration

1. Apply the selected patches to Apache version 2.4.26.
2. You can tune the patches' parameters through the `DEDOS_` compile-time parameters (passing the `-DDEDOS_*` parameters through `CFLAGS` would be an improvement).
3. Compile Apache using its standard workflow.

# License

[Apache 2.0](LICENSE)

# Author

[Nik Sultana](https://www.seas.upenn.edu/~nsultana/)
