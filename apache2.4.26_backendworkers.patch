diff --git a/server/mpm/worker/worker.c b/server/mpm/worker/worker.c
index d2147bf5d9..43e23c5f30 100644
--- a/server/mpm/worker/worker.c
+++ b/server/mpm/worker/worker.c
@@ -21,6 +21,10 @@
  * fixes those problems.
  */
 
+#define DEDOS_VERBOSE
+#define DEDOS_BLOCKING_PUSH 0
+#define DEDOS_BACK_Q_SIZE threads_per_child
+#define DEDOS_FRONT_WORKER_RATIO 0.5
 #include "apr.h"
 #include "apr_portable.h"
 #include "apr_strings.h"
@@ -30,6 +34,7 @@
 #include "apr_thread_mutex.h"
 #include "apr_proc_mutex.h"
 #include "apr_poll.h"
+#include "apr_queue.h"
 
 #include <stdlib.h>
 
@@ -60,6 +65,7 @@
 #include "http_config.h"        /* for read_config */
 #include "http_core.h"          /* for get_remote_host */
 #include "http_connection.h"
+#include "http_vhost.h" // NOTE nsultana: added this because needed prototype for "ap_update_vhost_given_ip"
 #include "ap_mpm.h"
 #include "mpm_common.h"
 #include "ap_listen.h"
@@ -135,6 +141,13 @@ static int resource_shortage = 0;
 static fd_queue_t *worker_queue;
 static fd_queue_info_t *worker_queue_info;
 
+static apr_queue_t * conn_queue;
+struct conn_queue_entry {
+    int prev_worker_id;
+    conn_rec *conn;
+    apr_pool_t *ptrans;
+};
+
 /* data retained by worker across load/unload of the module
  * allocated on first call to pre-config hook; located on
  * subsequent calls to pre-config hook
@@ -463,6 +476,7 @@ static void process_socket(apr_thread_t *thd, apr_pool_t *p, apr_socket_t *sock,
                            int my_child_num,
                            int my_thread_num, apr_bucket_alloc_t *bucket_alloc)
 {
+    apr_status_t rv;
     conn_rec *current_conn;
     long conn_id = ID_FROM_CHILD_THREAD(my_child_num, my_thread_num);
     ap_sb_handle_t *sbh;
@@ -473,8 +487,67 @@ static void process_socket(apr_thread_t *thd, apr_pool_t *p, apr_socket_t *sock,
                                             conn_id, sbh, bucket_alloc);
     if (current_conn) {
         current_conn->current_thread = thd;
-        ap_process_connection(current_conn, sock);
-        ap_lingering_close(current_conn);
+
+        // NOTE nsultana: code below based on ap_process_connection, except for the push to conn_queue
+        int rc;
+        ap_update_vhost_given_ip(current_conn);
+
+        rc = ap_run_pre_connection(current_conn, sock);
+        if (rc != OK && rc != DONE) {
+            current_conn->aborted = 1;
+        }
+
+        if (!current_conn->aborted) {
+            // NOTE nsultana: this was the line in ap_process_connection:
+            // ap_run_process_connection(current_conn);
+
+            struct conn_queue_entry * entry = apr_pcalloc(p, sizeof *entry);
+            entry->conn = current_conn;
+            entry->ptrans = p;
+            entry->prev_worker_id = my_thread_num;
+
+#ifdef DEDOS_VERBOSE
+            printf("worker %d pushed job %p (from %s) downstream at %ld\n", my_thread_num, entry,
+                current_conn->client_ip, apr_time_sec(apr_time_now()));
+#endif
+
+#if DEDOS_BLOCKING_PUSH
+            rv = apr_queue_push(conn_queue, entry);
+#else
+            rv = apr_queue_trypush(conn_queue, entry);
+#endif
+            // FIXME nsultana: check for APR_EINTR and APR_EOF
+            if (APR_EAGAIN == rv) {
+                // Queue is full, so we decide to end this connection and free up this worker
+                // (rather than have it block or wait).
+
+#ifdef DEDOS_VERBOSE
+                printf("worker %d (job %p) backend queue full, started lingering at %ld\n", my_thread_num,
+                    entry, apr_time_sec(apr_time_now()));
+#endif
+                ap_lingering_close(entry->conn);
+#ifdef DEDOS_VERBOSE
+                printf("worker %d (job %p) finished lingering, started clearing at %ld\n", my_thread_num,
+                    entry, apr_time_sec(apr_time_now()));
+#endif
+                apr_pool_clear(entry->ptrans);
+#ifdef DEDOS_VERBOSE
+                printf("worker %d (job %p) finished clearing at %ld\n", my_thread_num,
+                    entry, apr_time_sec(apr_time_now()));
+#endif
+            }
+
+            // In case we used apr_queue_push above and blocked, we check if
+            // the function returned because of exceptional circumstances.
+            if (rv != APR_SUCCESS) {
+                if (!workers_may_exit) {
+                    ap_log_error(APLOG_MARK, APLOG_CRIT, rv, ap_server_conf, APLOGNO() // FIXME nsultana: missing unique APLOGNO
+                                 "apr_queue_push/apr_queue_trypush failed");
+                }
+            }
+
+            ap_update_child_status(current_conn->sbh, SERVER_READY, NULL);
+        }
     }
 }
 
@@ -750,6 +823,80 @@ static void * APR_THREAD_FUNC listener_thread(apr_thread_t *thd, void * dummy)
     return NULL;
 }
 
+static void * APR_THREAD_FUNC backend_worker_thread(apr_thread_t *thd, void * dummy)
+{
+    proc_info * ti = dummy;
+    int process_slot = ti->pid;
+    int thread_slot = ti->tid;
+    apr_status_t rv;
+
+    free(ti);
+
+    ap_scoreboard_image->servers[process_slot][thread_slot].pid = ap_my_pid;
+    ap_scoreboard_image->servers[process_slot][thread_slot].tid = apr_os_thread_current();
+    ap_scoreboard_image->servers[process_slot][thread_slot].generation = retained->mpm->my_generation;
+    ap_update_child_status_from_indexes(process_slot, thread_slot,
+                                        SERVER_STARTING, NULL);
+
+    struct conn_queue_entry * entry;
+
+    while (!workers_may_exit) {
+        ap_update_child_status_from_indexes(process_slot, thread_slot,
+                                            SERVER_READY, NULL);
+backendworker_pop:
+        if (workers_may_exit) {
+            break;
+        }
+
+        rv = apr_queue_pop(conn_queue, (void **)&entry);
+
+        if (rv != APR_SUCCESS) {
+            // NOTE based on worker_thread()
+            if (APR_STATUS_IS_EOF(rv)) {
+                break;
+            }
+            else if (APR_STATUS_IS_EINTR(rv)) {
+                goto backendworker_pop;
+            }
+            else if (!workers_may_exit) {
+                ap_log_error(APLOG_MARK, APLOG_CRIT, rv, ap_server_conf, APLOGNO() // FIXME nsultana: no unique APLOGNO
+                             "apr_queue_pop failed");
+            }
+            continue;
+        }
+
+        // Propagate new worker info to the popped data.
+        ap_create_sb_handle((struct ap_sb_handle_t **)&(entry->conn->sbh), entry->ptrans, process_slot, thread_slot);
+#ifdef DEDOS_VERBOSE
+        printf("backendworker %d continued job %p from worker %d at %ld\n", thread_slot,
+            entry, entry->prev_worker_id, apr_time_sec(apr_time_now()));
+#endif
+
+        ap_run_process_connection(entry->conn);
+#ifdef DEDOS_VERBOSE
+        printf("backendworker %d (job %p) finished work, started lingering at %ld\n", thread_slot,
+            entry, apr_time_sec(apr_time_now()));
+#endif
+        ap_lingering_close(entry->conn);
+#ifdef DEDOS_VERBOSE
+        printf("backendworker %d (job %p) finished lingering, started clearing at %ld\n", thread_slot,
+            entry, apr_time_sec(apr_time_now()));
+#endif
+        apr_pool_clear(entry->ptrans);
+#ifdef DEDOS_VERBOSE
+        printf("backendworker %d (job %p) finished clearing at %ld\n", thread_slot,
+            entry, apr_time_sec(apr_time_now()));
+#endif
+    }
+
+    ap_update_child_status_from_indexes(process_slot, thread_slot,
+                                        dying ? SERVER_DEAD
+                                              : SERVER_GRACEFUL, NULL);
+
+    apr_thread_exit(thd, APR_SUCCESS);
+    return NULL;
+}
+
 /* XXX For ungraceful termination/restart, we definitely don't want to
  *     wait for active connections to finish but we may want to wait
  *     for idle workers to get out of the queue code and release mutexes,
@@ -835,11 +982,18 @@ worker_pop:
         is_idle = 0;
         worker_sockets[thread_slot] = csd;
         bucket_alloc = apr_bucket_alloc_create(ptrans);
+#ifdef DEDOS_VERBOSE
+        printf("worker %d got a job at %ld\n", thread_slot, apr_time_sec(apr_time_now()));
+#endif
         process_socket(thd, ptrans, csd, process_slot, thread_slot, bucket_alloc);
         worker_sockets[thread_slot] = NULL;
         requests_this_child--;
-        apr_pool_clear(ptrans);
-        last_ptrans = ptrans;
+        // NOTE nsultana: this was moved to the backend worker:
+        //apr_pool_clear(ptrans);
+        // NOTE nsultana: this was changed since we can't leave any pointers to
+        //      ptrans since it could create a race with downstream workers
+        //      using ptrans.
+        last_ptrans = NULL;
     }
 
     ap_update_child_status_from_indexes(process_slot, thread_slot,
@@ -901,10 +1055,16 @@ static void * APR_THREAD_FUNC start_threads(apr_thread_t *thd, void *dummy)
     int loops;
     int prev_threads_created;
 
+    int worker_threads_per_child = threads_per_child * DEDOS_FRONT_WORKER_RATIO;
+#ifdef DEDOS_VERBOSE
+    printf("threads_per_child=%d\n", threads_per_child);
+    printf("worker_threads_per_child=%d\n", worker_threads_per_child);
+#endif
+
     /* We must create the fd queues before we start up the listener
      * and worker threads. */
     worker_queue = apr_pcalloc(pchild, sizeof(*worker_queue));
-    rv = ap_queue_init(worker_queue, threads_per_child, pchild);
+    rv = ap_queue_init(worker_queue, worker_threads_per_child, pchild);
     if (rv != APR_SUCCESS) {
         ap_log_error(APLOG_MARK, APLOG_ALERT, rv, ap_server_conf, APLOGNO(03140)
                      "ap_queue_init() failed");
@@ -922,10 +1082,20 @@ static void * APR_THREAD_FUNC start_threads(apr_thread_t *thd, void *dummy)
     worker_sockets = apr_pcalloc(pchild, threads_per_child
                                         * sizeof(apr_socket_t *));
 
+    rv = apr_queue_create(&conn_queue, DEDOS_BACK_Q_SIZE, pchild);
+    if (rv != APR_SUCCESS) {
+        ap_log_error(APLOG_MARK, APLOG_ALERT, rv, ap_server_conf, APLOGNO() // FIXME nsultana: missing unique APLOGNO
+                     "apr_queue_create() failed");
+        clean_child_exit(APEXIT_CHILDFATAL);
+    }
+
     loops = prev_threads_created = 0;
     while (1) {
         /* threads_per_child does not include the listener thread */
-        for (i = 0; i < threads_per_child; i++) {
+        for (i = 0; i < worker_threads_per_child; i++) {
+#ifdef DEDOS_VERBOSE
+            printf("Starting worker on thread %d\n", i);
+#endif
             int status = ap_scoreboard_image->servers[my_child_num][i].status;
 
             if (status != SERVER_GRACEFUL && status != SERVER_DEAD) {
@@ -953,6 +1123,38 @@ static void * APR_THREAD_FUNC start_threads(apr_thread_t *thd, void *dummy)
             }
             threads_created++;
         }
+        // Start backend workers.
+        for (i = threads_created; i < threads_per_child; i++) {
+#ifdef DEDOS_VERBOSE
+            printf("Starting backendworker on thread %d\n", i);
+#endif
+            int status = ap_scoreboard_image->servers[my_child_num][i].status;
+
+            if (status != SERVER_GRACEFUL && status != SERVER_DEAD) {
+                continue;
+            }
+
+            my_info = (proc_info *)ap_malloc(sizeof(proc_info));
+            my_info->pid = my_child_num;
+            my_info->tid = i;
+            my_info->sd = 0;
+
+            /* We are creating threads right now */
+            ap_update_child_status_from_indexes(my_child_num, i,
+                                                SERVER_STARTING, NULL);
+            /* We let each thread update its own scoreboard entry.  This is
+             * done because it lets us deal with tid better.
+             */
+            rv = apr_thread_create(&threads[i], thread_attr,
+                                   backend_worker_thread, my_info, pchild);
+            if (rv != APR_SUCCESS) {
+                ap_log_error(APLOG_MARK, APLOG_ALERT, rv, ap_server_conf, APLOGNO() // FIXME nsultana: missing unique APLOGNO
+                             "apr_thread_create: unable to create backend worker thread");
+                /* let the parent decide how bad this really is */
+                clean_child_exit(APEXIT_CHILDSICK);
+            }
+            threads_created++;
+        }
         /* Start the listener only when there are workers available */
         if (!listener_started && threads_created) {
             create_listener_thread(ts);
